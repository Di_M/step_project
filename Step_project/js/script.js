//Helpers
let clickedLoadMore = false;
const numberImagesBegin = 11,
    numberImageMiddle = 23;

const checkWorkImages = () => {
    $('.work__image').not('.work_image-hide').each(function (i) {
        if (i > numberImagesBegin) {
            $(this).addClass('work_image-hide');
            $('.works__btn').show();
        } else {
            $('.works__btn').hide();
        }
    });
};

const checkWorksBtnLoadMore = () => {
    $('[data-works]').each(function () {
        if($(this).hasClass('work_theme_active')) {
            if ($(this).data('works') === "all") {
                let dataWorksImage = $('.work__image').removeClass('work_image-hide');
                if (dataWorksImage.length < numberImageMiddle) {
                    $('.works__btn').hide();
                }
                dataWorksImage.each(function (i) {
                    if(clickedLoadMore === false) {
                        if (i > numberImageMiddle) {
                            $(this).addClass('work_image-hide');
                        }
                    } else {
                        $('.works__btn').hide();
                    }
                });
            } else {
                let dataWorksImage = $(`[data-works-image = "${$(this).data('works')}"]`).removeClass('work_image-hide');
                if (dataWorksImage.length < numberImageMiddle) {
                    $('.works__btn').hide();
                }
                dataWorksImage.each(function (i) {
                    if(clickedLoadMore === false) {
                        if (i > numberImageMiddle) {
                            $(this).addClass('work_image-hide');
                        }
                    } else {
                        $('.works__btn').hide();
                    }
                });
            }
        }
    });
    clickedLoadMore = true;
    $('h2.animate').css("display", "none");
    $("html,body").css("overflow", "auto");
};



//Tabs in Service block

$('[data-tab]').click(function () {
    $(this)
        .addClass('service_theme_active')
        .siblings('[data-tab]')
        .removeClass('service_theme_active');
    $(`[data-content = "${$(this).data('tab')}"]`)
        .addClass('services__description-box_active')
        .siblings('[data-content]')
        .removeClass('services__description-box_active');
});





//Filter Works block


$('[data-works]').click(function () {
    clickedLoadMore = false;
    $(this).addClass('work_theme_active').siblings('[data-works]').removeClass('work_theme_active');
    $('.work__image').removeClass('work_image-hide');
    if ($(this).data('works') === "all") {
        checkWorkImages();
    } else {
        $('[data-works-image]').addClass('work_image-hide');
        $(`[data-works-image = "${$(this).data('works')}"]`).removeClass('work_image-hide');
        checkWorkImages();
    }
});



// Works Button "Load More"


$('.works__btn').click(function () {
    $('h2.animate').css('display', 'flex');
    $("html,body").css("overflow", "hidden");
    setTimeout(checkWorksBtnLoadMore, 3000);
});



//Review Slider

let slideIndex = 3;
const lastSlideNumber = 4;

const showActiveSlide = (n) => {
    if (n > $('.reviews__list li').length) {
        slideIndex = 1;
    } else if(n <= 0) {
        slideIndex = lastSlideNumber;
    }
    $('.reviews__list li').removeClass('review__list_active-item');
    let activeSlide = $('.reviews__list li').eq(slideIndex - 1);
    activeSlide.addClass('review__list_active-item');
    $(`[data-author-block = "${activeSlide.data('author-item')}"]`)
        .addClass('reviews__author-block_active')
        .siblings('[data-author-block]')
        .removeClass('reviews__author-block_active');
};

const nextSlide = (n) => {
    showActiveSlide(slideIndex += n);
};

const prevSlide = (n) => {
    showActiveSlide(slideIndex -= n);
};



$('.reviews__prev').click(function () {
    prevSlide(1);
});

$('.reviews__next').click(function () {
    nextSlide(1);
});

$('[data-author-item]').click(function () {
    slideIndex = $(this).data('author-item');
    showActiveSlide(slideIndex);
});



//Gallery with plugin Masonry

const masonryFunc = () => {
    $('.gallery__content').masonry({
        // options
        itemSelector: '.gallery__images-block',
        columnWidth: 370,
        gutter: 10,
        percentPosition: true
    });
};

$(document).ready(function () {
    $('.gallery__content').imagesLoaded(function () {
        masonryFunc();
    });
});



//Gallery Button "Load more"

const showGalleryImages = () => {
    $('.gallery__images-block').removeClass('hide');
    $('.gallery__btn').hide();
        masonryFunc();
    $('h2.animate').css("display", "none");
    $("html,body").css("overflow", "auto");
};


$('.gallery__btn').click(function () {
    $('h2.animate').css('display', 'flex');
    $("html,body").css("overflow", "hidden");
    setTimeout(showGalleryImages, 3000);
});


//Gallery "Open image"

$('.gallery__image-size-big').click(function () {
    let imageBigger = $(this).parent().prev().attr('src');
    $('.biger-image').css('display', 'flex');
    $('.biger-image').find('img').attr('src', imageBigger);
    $("html,body").css("overflow", "hidden");
});

$('.biger-image').click(function () {
    $(this).css('display', 'none');
    $('.biger-image').find('img').attr('src', '');
    $("html,body").css("overflow", "auto");
});

